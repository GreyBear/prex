package com.prex.common.message.sms.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Classname PrexSmsProperties
 * @Description prex 短信平台配置
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-26 15:05
 * @Version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "prex.sms")
public class PrexSmsProperties {

    /**
     * 阿里云短信
     */
    AliYunSmsProperties aliyun = new AliYunSmsProperties();

    /**
     * 秒滴云短信
     */
    MiaoDiYunSmsProperties miaodiyun = new MiaoDiYunSmsProperties();



}
