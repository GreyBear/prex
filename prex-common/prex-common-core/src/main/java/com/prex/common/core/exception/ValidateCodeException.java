package com.prex.common.core.exception;

/**
 * @Classname ValidateCodeException
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-04 10:45
 * @Version 1.0
 */
public class ValidateCodeException extends Exception {

    private static final long serialVersionUID = -7285211528095468156L;

    public ValidateCodeException() {
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
