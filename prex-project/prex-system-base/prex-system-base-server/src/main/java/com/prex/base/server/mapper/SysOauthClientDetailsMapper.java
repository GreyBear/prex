package com.prex.base.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prex.base.api.entity.SysOauthClientDetails;

/**
 * @Classname SysOauthClientDetailsMapper
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-05 11:41
 * @Version 1.0
 */
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
