package com.prex.client.controller;


import com.prex.client.service.IClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @ClassName ClientController
 * @Description TODO
 * @Author yanlin
 * @Version v1.0
 * @Date 2019-08-08 2:38 PM
 **/
@RestController
@RequestMapping("/client")
@RefreshScope
@Api("client")
public class ClientController {

    @Autowired
    private IClientService clientService;

    @Value("${spring.test}")
    private String type;

    /**
     * 根据token获取登录用户主体相关信息（用户名，token，角色。。。等）
     *
     * @return
     */
//    @GetMapping("/principal")
//    public Principal user(@RequestParam(required = false) String clientName) {
//        System.err.println(clientName + ":调用了principal");
//        SecurityContext context = SecurityContextHolder.getContext();
//        return context.getAuthentication();
//    }
    @ApiOperation(value = "获取请求头")
    @GetMapping("/test")
    public String test(HttpServletRequest request) {
        System.out.println("----------------header----------------");
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            System.out.println(key + ": " + request.getHeader(key));
        }
        System.out.println("----------------header----------------");
        return "hello!";
    }
    @ApiOperation(value = "获取配置")
    @GetMapping("/config")
    public String config() {
        return type;
    }

    @ApiOperation(value = "sentinel")
    @GetMapping("/sentinel")
    public void sentinel() {
        String sentinel = "sentinel";
        clientService.clientTest(sentinel);
    }

}
